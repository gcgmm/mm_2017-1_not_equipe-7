﻿using System;

public static class CoordinatesDistanceExtension {

    public static double WORLD_RADIUS_KILOMETERS = 6371.0f;
    public static double WORLD_RADIUS_METERS = WORLD_RADIUS_KILOMETERS * 1000.0f;
    public static double WORLD_20M_LAT = ((-26.905567 - (-26.905487)) * 2);
    public  static double WORLD_20m_LON = ((-49.078021 - (-49.077988)) * 2);


    public static double DistanceTo(GeoPosition baseCoordinates, GeoPosition targetCoordinates, UnitOfLength unitOfLength) {
        var baseRad = Math.PI * baseCoordinates.Latitude / 180;
        var targetRad = Math.PI * targetCoordinates.Latitude / 180;
        var theta = baseCoordinates.Longitude - targetCoordinates.Longitude;
        var thetaRad = Math.PI * theta / 180;

        double dist =
            Math.Sin(baseRad) * Math.Sin(targetRad) + Math.Cos(baseRad) *
            Math.Cos(targetRad) * Math.Cos(thetaRad);
        dist = Math.Acos(dist);

        dist = dist * 180 / Math.PI;
        dist = dist * 60 * 1.1515;

        return unitOfLength.ConvertFromMiles(dist);
    }

    public static double refDistanceMeters(GeoPosition from, GeoPosition to) {
        double step1 = Math.Sqrt(1.0d - (Math.Pow(Math.Sin((DegreeToRadian(to.Latitude) - DegreeToRadian(from.Latitude)) / 2.0d), 2.0d)
                                          + Math.Cos(DegreeToRadian(from.Latitude)) * Math.Cos(DegreeToRadian(to.Latitude))
                                          * Math.Pow(Math.Sin((DegreeToRadian(to.Longitude) - DegreeToRadian(from.Longitude)) / 2.0d), 2.0d)));

        double step2 = Math.Sqrt((Math.Pow(Math.Sin((DegreeToRadian(to.Latitude) - DegreeToRadian(from.Latitude)) / 2.0d), 2.0d)
                                  + Math.Cos(DegreeToRadian(from.Latitude)) * Math.Cos(DegreeToRadian(to.Latitude))
                                  * Math.Pow(Math.Sin((DegreeToRadian(to.Latitude) - DegreeToRadian(from.Latitude)) / 2.0d), 2.0d)));

        double step3 = Math.Atan(step2 / step1);

        return WORLD_RADIUS_METERS * 2.0f * step3;
    }

    private static double DegreeToRadian(double angle) {
        return Math.PI * angle / 180.0;
    }
}