﻿using UnityEngine;
using System.Collections;
using System.Diagnostics;

public class GPS : MonoBehaviour {

    public double latitude;
    public double longitude;

    private double lastLatitude;
    private double lastLongitude;

    public static GPS Instance { get; set; }

    public long elapsedTimeInMillis;
    public bool isAvailable { get; set; }
    Stopwatch watch = new Stopwatch();

    private void Start() {

        Instance = this;
        isAvailable = false;
        DontDestroyOnLoad(gameObject);
        StartCoroutine(StartLocationService());
    }

    private IEnumerator StartLocationService() {
        if (!Input.location.isEnabledByUser) {
            UnityEngine.Debug.Log("User has not enable GPS");
            yield break;
        }

        Input.location.Start(3f, 0.5f);
        int maxWait = 20;

        while (Input.location.status == LocationServiceStatus.Initializing && maxWait > 0) {
            yield return new WaitForSeconds(1);
            maxWait--;
        }

        if (maxWait <= 0) {
            UnityEngine.Debug.Log("Timed out");
            yield break;
        }

        if (Input.location.status == LocationServiceStatus.Failed) {
            UnityEngine.Debug.Log("Unable to get device location");
            yield break;
        }

        watch.Start();
        isAvailable = true;
        latitude = Input.location.lastData.latitude;
        longitude = Input.location.lastData.longitude;

        yield break;
    }

    private void Update() {

        elapsedTimeInMillis = watch.ElapsedMilliseconds;

        if (isAvailable) {
            lastLatitude = Input.location.lastData.latitude;
            lastLongitude = Input.location.lastData.longitude;

            if (lastLatitude != latitude || lastLongitude != longitude) {
                latitude = lastLatitude;
                longitude = lastLongitude;
                watch.Reset();
                watch.Start();
            }
        }
    }
}
