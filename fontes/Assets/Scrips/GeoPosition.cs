﻿public class GeoPosition{

    public double Latitude { get;  set; }
    public double Longitude { get; set; }

    public GeoPosition(double latitude, double longitude) {
        Latitude = latitude;
        Longitude = longitude;
    }
}