﻿using UnityEngine;

public class PlayerLocation : MonoBehaviour {

    private GeoPosition loc { get; set; }

    public PlayerLocation() {
        loc = new GeoPosition(0, 0);
    }

    void Update() {
        loc.Latitude = GPS.Instance.latitude;
        loc.Longitude = GPS.Instance.longitude;
    }

}
