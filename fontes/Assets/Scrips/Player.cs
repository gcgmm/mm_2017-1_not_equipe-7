﻿using UnityEngine;

public class Player : MonoBehaviour {

    public static Player Instance { get; set; }

    public int correctAnswers { get; set; }
    public int wrongAnswers { get; set; }
    public Waypoint currentWaypoint { get; set; }
    public PlayerLocation loc { get; set; }

    private void Start() {
        Instance = this;
        currentWaypoint = null;
        loc = new PlayerLocation();
        correctAnswers = 0;
        wrongAnswers = 0;
    }
}
