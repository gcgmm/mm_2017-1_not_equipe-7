﻿using System;
using System.Collections.Generic;
using UnityEngine;

public class QuestGenerator : MonoBehaviour {

    private List<String> perguntas;
    private List<String> respostas;

    public QuestGenerator(int quant, int dif) {
        perguntas = new List<string>();
        respostas = new List<string>();
        gerar(quant, dif);
    }

    public void gerar(int quant, int dif) {
		System.Random rnd = new System.Random();

        int a = 0;
        int b = 0;

        for (int x = 0; x < quant; x++) {
			
            if (dif == 1) {
                a = rnd.Next(1, 10);
                b = rnd.Next(1, 10);
            } else if (dif == 2) {
                a = rnd.Next(1, 99);
                b = rnd.Next(1, 99);
            } else if (dif == 3) {
                a = rnd.Next(1, 999);
                b = rnd.Next(1, 999);
            }

            string pergunta = montaPergunta(a, b);
            perguntas.Add(pergunta);
            string resposta = montaResposta(a, b);
            respostas.Add(resposta);
        }

    }

    public string resposta(int i ) {
        return respostas[i];
    }

    public string pergunta(int i) {
        return perguntas[i];
    }


    private string montaPergunta(int a, int b) {
        int result = a * b;
		string retorno = "Qual o resultado da seguinte multiplicação? "+Environment.NewLine+"\t"+"\t"+"\t"+"\t"+"\t"+"\t"+Convert.ToString(a) + " x " + Convert.ToString(b);

        return retorno;
    }

    private string montaResposta(int a,int b) {
        return Convert.ToString(a * b);
    }

}
