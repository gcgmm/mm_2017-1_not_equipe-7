﻿public class Waypoint {

    public double Latitude { get; set; }
    public double Longitude { get; set; }
    public GeoPoint Loc { get; set; }
    public string Question { get; set; }
    public string Answer { get; set; }
    public string Name { get; set; }
    
    public Waypoint(GeoPosition geopos,string name) {
        this.Latitude = geopos.Latitude;
        this.Longitude = geopos.Longitude;
        Loc = new GeoPoint((float)geopos.Latitude, (float)geopos.Longitude);
        this.Name = name;
    }
}