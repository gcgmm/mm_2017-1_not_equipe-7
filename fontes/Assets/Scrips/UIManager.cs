﻿using System;
using UnityEngine;
using UnityEngine.UI;

public class UIManager : MonoBehaviour {

    public Text perguntaText;
	public Text respostaText;
	private bool flag;

	private void Update () {
		if(!flag)
		perguntaText.text = GameManager.Instance.currentWaypointQuestion ();
	}

	public void checkAnswer(){
		
		if (Convert.ToInt32(respostaText.text) == Convert.ToInt32((GameManager.Instance.currentWaypointAnswer()))) {			
			GameManager.Instance.nextWaypoint ();
			GameManager.Instance.incrementAcertosCounter ();
			GameManager.Instance.respondeu = false;
			Application.LoadLevel (1);			
			flag = true;
		}
	}
}
