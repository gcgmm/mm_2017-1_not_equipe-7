﻿using System;
using System.Diagnostics;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{

	public static GameManager Instance;

	public GameObject player;
	public GameObject waypoint;
	public Text distanceText;
	public Text acertosCounter;
	public GameObject WaypointText;

	private long elapsedTimeInMillis;
	private int samplePool;
	private int waypoints;
	private Vector3 originalPosition;
	private static bool init;
	private double lat, lastLat, longi, lastLongi;
	private List<double> latitudeList;
	private List<double> longitudeList;
	private int acertosC;
	private Stopwatch watch;
	private string displayText;
	private int i;
	[HideInInspector]
	public bool respondeu;
	private float distanceFloat;

	private void Start ()
	{		
		if (Instance != null) {
			GameObject.Destroy (gameObject);		
		} else {
			DontDestroyOnLoad (gameObject);
			Instance = this;
		}
		i = 0;
		waypoints = 10;
		latitudeList = new List<double> ();
		longitudeList = new List<double> ();
		lat = 0;
		lastLat = 0;
		longi = 0;
		lastLongi = 0;
		samplePool = 2;
		acertosCounter.text = Convert.ToString (acertosC);	
		watch = new Stopwatch ();
	}

		

	public void resetWatch ()
	{
		watch.Reset ();
		watch.Start ();
	}

	public void incrementAcertosCounter ()
	{
		acertosC++;
		acertosCounter.text = Convert.ToString (acertosC);
	}

	private void updateWaypointUI(){
		
		if (distanceFloat < 15) {
			WaypointText.GetComponent<TextMesh> ().text = WaypointManager.Instance.CurrentWaypoint.Name;
		} else {
			WaypointText.GetComponent<TextMesh> ().text = "";
		}

	}

	private void Update ()
	{
		updateLatLong ();

		initialize (lat, longi);
				
		calculateDistance ();

		updateMap ();

		updateWaypointUI ();

		checkDistance ();

		updateUI ();

	}

	private void updateMap(){
		waypoint.GetComponent<ObjectPosition> ().setPositionOnMap (WaypointManager.Instance.CurrentWaypoint.Loc);
		waypoint.GetComponent<TextMesh> ().text = WaypointManager.Instance.CurrentWaypoint.Name;
	}

	private void updateLatLong(){
		if (GPS.Instance != null) {
			lat = GPS.Instance.latitude;
			longi = GPS.Instance.longitude;
		}
	}

	private void updateUI(){
		string display = String.Format ("{0:0.}",distanceFloat);

		distanceText.text = display + "M";
	}

	public string currentWaypointQuestion(){
		return WaypointManager.Instance.CurrentWaypoint.Question;
	}

	public void nextWaypoint(){
		WaypointManager.Instance.nextWaypoint ();
	}

	private void calculateDistance(){
		//atualiza distancia
		distanceFloat = (float)CoordinatesDistanceExtension.refDistanceMeters (new GeoPosition (lat, longi), new GeoPosition (WaypointManager.Instance.CurrentWaypoint.Latitude, WaypointManager.Instance.CurrentWaypoint.Longitude));
	}

	public string currentWaypointAnswer(){
		return WaypointManager.Instance.CurrentWaypoint.Answer;
	}

	private void checkDistance ()
	{
		elapsedTimeInMillis = watch.ElapsedMilliseconds;

		if (elapsedTimeInMillis > 5000 && !respondeu) {
			if (distanceFloat < 15) {
			
				respondeu = true;
				Application.LoadLevel (2);	
			}				
		}
	}

	private void initialize (double lat, double longi)
	{
		if (i < 1) {
			if (lat == 0 || longi == 0) {
				return;
			}
			if (lat != 0 && longi != 0 && latitudeList.Count <= samplePool && longitudeList.Count <= samplePool) {
				if (lastLat != lat) {
					lastLat = lat;
					if (latitudeList.Count < samplePool && longitudeList.Count < samplePool) {
						latitudeList.Add (lastLat);
						longitudeList.Add (longi);
					}
				} else if (lastLongi != longi) {
					lastLongi = longi;
					if (latitudeList.Count < samplePool && longitudeList.Count < samplePool) {
						longitudeList.Add (lastLongi);
						latitudeList.Add (lastLat);
					}
				}
				if (latitudeList.Count >= samplePool && longitudeList.Count >= samplePool) {
					WaypointManager.Instance.gerarWPs (averageLatitude (), averageLongitude ());
					watch.Start ();
					init = true;
					i++;
				}
			}
		}
	}

	private double averageLatitude ()
	{
		double r = 0;

		foreach (var lat in latitudeList) {
			r += lat;
		}

		return r / latitudeList.Count;
	}

	private double averageLongitude ()
	{
		double r = 0;

		foreach (var longi in longitudeList) {
			r += longi;
		}

		return r / longitudeList.Count;
	}

	private string closestWP ()
	{

		double distance = 1000;
		double d = 0;
		string name = null;
		if (WaypointManager.Instance.Waypoints.Count > 0) {
			foreach (var wp in WaypointManager.Instance.Waypoints) {
				d = CoordinatesDistanceExtension.refDistanceMeters (new GeoPosition (lat, longi), new GeoPosition (wp.Latitude, wp.Longitude));
				if (d < distance) {
					distance = d;
					name = wp.Name;
				}
			}
		}
		return name;
	}
}
