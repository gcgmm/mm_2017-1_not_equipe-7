﻿using System.Collections.Generic;
using UnityEngine;

public class WaypointManager : MonoBehaviour
{
	public static WaypointManager Instance;
	public List<Waypoint> Waypoints { get; set; }
	public Waypoint CurrentWaypoint { get; set; }

	public int indexCurrentWaypoint;
	private QuestGenerator gen;

	private void Start(){
		if (Instance != null) {
			GameObject.Destroy (gameObject);		
		} else {
			GameObject.DontDestroyOnLoad (gameObject);
			Instance = this;
		}
		Waypoints = new List<Waypoint> ();
		gen = new QuestGenerator (5, 1);
	}

	public void addWaypoint (Waypoint wp)
	{
		Waypoints.Add (wp);
	}

	public void nextWaypoint ()
	{		
		if (indexCurrentWaypoint < Waypoints.Count - 1) {
			indexCurrentWaypoint++;
			CurrentWaypoint = Waypoints [indexCurrentWaypoint];
		}
	}

	public void gerarWPs (double initialLat, double initialLong)
	{
		
		Waypoint wp1 = new Waypoint (new GeoPosition (initialLat, initialLong), "Waypoint 1");
		wp1.Question = gen.pergunta (0);
		wp1.Answer = gen.resposta (0);
		addWaypoint (wp1);

		Waypoint wp2 = new Waypoint (new GeoPosition (initialLat + CoordinatesDistanceExtension.WORLD_20M_LAT, initialLong + CoordinatesDistanceExtension.WORLD_20m_LON), "Waypoint 2");
		wp2.Question = gen.pergunta (1);
		wp2.Answer = gen.resposta (1);
		addWaypoint (wp2);

		Waypoint wp3 = new Waypoint (new GeoPosition (initialLat - CoordinatesDistanceExtension.WORLD_20M_LAT, initialLong + CoordinatesDistanceExtension.WORLD_20m_LON), "Waypoint 3");
		wp3.Question = gen.pergunta (2);
		wp3.Answer = gen.resposta (2);
		addWaypoint (wp3);

		Waypoint wp4 = new Waypoint (new GeoPosition (initialLat - CoordinatesDistanceExtension.WORLD_20M_LAT, initialLong - CoordinatesDistanceExtension.WORLD_20m_LON), "Waypoint 4");
		wp4.Question = gen.pergunta (3);
		wp4.Answer = gen.resposta (3);
		addWaypoint (wp4);

		Waypoint wp5 = new Waypoint (new GeoPosition (initialLat + CoordinatesDistanceExtension.WORLD_20M_LAT, initialLong - CoordinatesDistanceExtension.WORLD_20m_LON), "Waypoint 5");
		wp5.Question = gen.pergunta (4);
		wp5.Answer = gen.resposta (4);
		addWaypoint (wp5);

		CurrentWaypoint = Waypoints [0];
		indexCurrentWaypoint = 0;
	}
}
